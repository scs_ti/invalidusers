import ldap.filter
import cx_Oracle


def main():
    def objldap(uri, user_dn, password):
        """
        Retorna um objeto ldap para fazer a busca por usuários inativos
        :param uri: string
        :param user_dn: string
        :param password: string
        :return ldap.ldapobject.SimpleLDAPObject :
        """

        l = ldap.initialize(uri)
        user_dn = user_dn
        password = password
        l.simple_bind_s(user_dn, password)
        return l

    # filter: caminho de onde será buscados os usuários
    filter = "(memberof=CN=INATIVO,OU=USUARIOS_INATIVOS,DC=ascs,DC=intra)"

    obj = objldap("ldap://10.201.0.250:389", r"mvaut@ascs.intra", "Mudar@123")
    res = obj.search_s("OU=USUARIOS_INATIVOS,DC=ascs,DC=intra", ldap.SCOPE_SUBTREE, filter)

    # dsn = "192.168.90.231/TRNME"
    dsn = "10.200.0.211/PRDME"
    for i, r in res:
        cd_usuario = str(r['sAMAccountName'][0].decode("utf-8")).upper()
        with cx_Oracle.connect(user="dbaps", password="dbapssta368", dsn=dsn, encoding="UTF-8") as connection:
            cursor = connection.cursor()
            cursor.callproc('INTEGRA_AD_SGU_INATIVA', ['{}'.format(cd_usuario)])
            print("Usuário " + cd_usuario + " inativado! ")


if __name__ == '__main__':
    main()
